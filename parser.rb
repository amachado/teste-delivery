require 'json'
require 'httparty'

Object.define_method(:to_currency) do
  if self.is_a?(Float)
    sub_total = self.to_s
    if sub_total.split(".")[1].size == 1
      return sub_total + "0"
    else
      return sub_total
    end
  elsif self.is_a?(Integer)
    return self.to_s + ".00"
  else
    raise "erro"
  end
end

String.define_method(:state_name_to_initials) do
  state = self.downcase
  case state
  when "acre", "ac"
    return "AC"
  when "alagoas", "al"
    return "AL"
  when "amapa", "amapá", "ap"
    return "AP"
  when "amazonas", "am"
    return "AM"
  when "bahia", "ba"
    return "BA"
  when "ceara", "ceará", "ce"
    return "CE"
  when "espirito santo", "espírito santo", "es"
    return "ES"
  when "goias", "goiás", "go"
    return "go"
  when "maranhao", "maranhão", "ma"
    return "MA"
  when "mato grosso", "mt"
    return "MT"
  when "mato grosso do sul", "ms"
    return "MS"
  when "minas gerais", "mg"
    return "MG"
  when "para", "pará", "pa"
    return "PA"
  when "paraiba", "paraíba", "pb"
    return "PB"
  when "parana", "paraná", "pr"
    return "PR"
  when "pernanbuco", "pe"
    return "PE"
  when "piaui", "piauí", "pi"
    return "PI"
  when "rio de janeiro", "rj"
    return "RJ"
  when "rio grande do norte", "rn"
    return "RN"
  when "rio grande do sul", "rs"
    return "RS"
  when "rondonia", "rondônia", "ro"
    return "RO"
  when "roraima", "rr"
    return "RR"
  when "santa catarina", "sc"
    return "SC"
  when "sao paulo", "são paulo", "sp"
    return "SP"
  when "sergipe", "se"
    return "SE"
  when "tocantins", "to"
    return "TO"
  when "distrito federal", "df"
    return "DF"
  else
    raise "erro"
  end
end

json_data = File.read("input.json")
hash = JSON.parse(json_data)

# hash.each { |i|
#   puts i
# }

output = Hash.new

begin
  hash["id"].is_a?(Integer) ? output["externalCode"] = hash["id"].to_s : raise("Erro aqui")
  hash["store_id"].is_a?(Integer) ? output["storeId"] = hash["store_id"] : raise("Erro aqui")
  output["subTotal"] = hash["total_amount"].to_currency
  output["deliveryFee"] = hash["total_shipping"].to_currency
  output["total"] = hash["total_amount_with_shipping"].to_currency
  output["country"] = hash["shipping"]["receiver_address"]["country"]["id"]
  output["state"] = hash["shipping"]["receiver_address"]["state"]["name"].state_name_to_initials
  output["city"] = hash["shipping"]["receiver_address"]["city"]["name"]
  output["district"] = hash["shipping"]["receiver_address"]["neighborhood"]["name"]
  output["street"] = hash["shipping"]["receiver_address"]["street_name"]
  output["complement"] = hash["shipping"]["receiver_address"]["comment"]
  output["latitude"] = hash["shipping"]["receiver_address"]["latitude"]
  output["longitude"] = hash["shipping"]["receiver_address"]["longitude"]
  output["dtOrderCreate"] = DateTime.parse(hash["date_created"]).strftime('%FT%TZ')
  output["postalCode"] = hash["shipping"]["receiver_address"]["zip_code"]
  output["number"] = hash["shipping"]["receiver_address"]["street_number"]
  output["customer"] = {
      "externalCode" => hash["buyer"]["id"].to_s,
      "name" => hash["buyer"]["nickname"],
      "email" => hash["buyer"]["email"],
      "contact" => hash["buyer"]["phone"]["area_code"].to_s + hash["buyer"]["phone"]["number"]
  }

  output["items"] = []
  hash["order_items"].each { |i|
    output["items"].push(
        {
            "externalCode" => i["item"]["id"],
            "name" => i["item"]["title"],
            "price" => i["unit_price"],
            "quantity" => i["quantity"],
            "total" => i["full_unit_price"],
            "subItems" => []
        }
    )
  }

  output["payments"] = []
  hash["payments"].each { |p|
    output["payments"].push(
        {
            "type" => p["payment_type"].upcase,
            "value" => p["total_paid_amount"]
        }
    )
  }

  output["total_shipping"] = hash["total_shipping"]
rescue StandardError => e
  puts e.message
  puts e.backtrace.inspect
end

# output["latitude"] = hash["store_id"]
# output["longitude"] = hash["store_id"]

File.open("output.json","w") do |f|
  f.write(JSON.pretty_generate(output))
end

response = HTTParty.post(
    'https://delivery-center-recruitment-ap.herokuapp.com/',
    :body => output.to_json,
    :headers => {
        'Content-Type' => 'application/json',
        'X-Sent' => DateTime.now.strftime('%Hh%M - %d/%m/%y')
    }
)
puts 'Response code: ' + response.code.to_s
puts response.body


